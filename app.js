﻿//app.js
const isJs = require("is_js"),
CountingSheep = function (aNumber) {
	let imAsleep = false,
	haveImsomnia = false;

	let addUnseenDigit = (num, digits) => {
		let canAddDigit = isJs.under(digits.length, 10);
		if (canAddDigit === true && isJs.not.inArray(num, digits)) {
			digits.push(num);
		}
		if (isJs.within(digits.length, 1, 10)) {
			digits.sort();
		}
		return canAddDigit === true;
	},
	_haveSeenAllTenDigits = (array) => {
		const tenDigits = '0,1,2,3,4,5,6,7,8,9';
		array.sort();
		const seen = array.join(',');
		return isJs.equal(seen, tenDigits);
	},
	_addUnseenDigits = (inputDigits, array) => {
		for (var i = 0, len = inputDigits.length; i < len; i += 1) {
			let added = addUnseenDigit(inputDigits[i], array);
			if (added === false) {
				break;
			}
		}
	},
	_processNumber = () => {
		const limit = 100;
		let count = 0;
		let seenDigits = [];
		while (limit > count) {
			let pivot = (count + 1) * aNumber,
			numAsDigits = _splitNumberAsDigits(pivot);
			_addUnseenDigits(numAsDigits, seenDigits);
			seenDigits.sort();
			if (_haveSeenAllTenDigits(seenDigits)) {
				break;
			} else {
				count++;
			}
		}
		imAsleep = _haveSeenAllTenDigits(seenDigits);
		haveImsomnia = !_haveSeenAllTenDigits(seenDigits) && count >= limit;
	},
	_splitNumberAsDigits = (num) => {
		let numberAsString = num.toString();
		let output = [];
		for (var i = 0, len = numberAsString.length; i < len; i += 1) {
			output.push(+numberAsString.charAt(i));
		}
		return output;
	};
	/*do the magic!!*/
	_processNumber();
	return {
		asleep: () => {
			return imAsleep;
		},
		imsomnia: () => {
			return haveImsomnia;
		}
	};
};
module.exports = CountingSheep;
