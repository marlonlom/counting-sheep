﻿const expect = require('chai').expect, CountingSheep = require("../app");
describe('Counting sheeep problem unit tests', function () {
	it('Beatrix trotter should fall asleep', function () {
		const bleatrixTrotter = new CountingSheep(1692);
		expect(bleatrixTrotter.asleep()).to.be.true;
		expect(bleatrixTrotter.imsomnia()).to.be.false;
	});
	it('Beatrix trotter should have imsomnia', function () {
		const bleatrixTrotter = new CountingSheep(0);
		expect(bleatrixTrotter.asleep()).to.be.false;
		expect(bleatrixTrotter.imsomnia()).to.be.true;
	});
	const readDataset = (inputFile) => {
		let contents = require('fs').readFileSync(inputFile, 'utf8'),
		numbers = contents.split('\n'),
		countAsleep = 0,
		countImsomnia = 0;
		numbers.forEach((item, position) => {
			let inputNum = Number(numbers[position]),
			sheep = new CountingSheep(inputNum);
			if (sheep.asleep()) {
				countAsleep++;
			}
			if (sheep.imsomnia()) {
				countImsomnia++;
			}
		});
		return {
			countImsomnia: countImsomnia,
			countAsleep: countAsleep
		};
	};
	it('The large dataset must contain 100 asleep and 2 sheeps with insomnia', function () {
		let countsLarge = readDataset('./test/A-large-practice.in');
		expect(countsLarge.countAsleep).gt(0);
		expect(countsLarge.countImsomnia).gt(0);
	});
	it('The small dataset must contain 100 asleep and 2 sheeps with insomnia', function () {
		let countsSmall = readDataset('./test/A-small-practice.in');
		expect(countsSmall.countAsleep).gt(0);
		expect(countsSmall.countImsomnia).gt(0);
	});
});
